package com.kaewmanee.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

class MyFrame extends JFrame{ //สร้างในfile เดียวกันได้แต่ต้องไม่มีpublic
    JButton button;
    public MyFrame (){ 
        super("First JFrame");
        button = new JButton("Click");
        button.setBounds(180,100,100,40); //กว้าง 100 สูง 40
        this.setLayout(null);
        this.add(button);
        this.setSize(500,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
public class Simple2 {
    public static void main(String[] args) {
        MyFrame frame = new MyFrame();
        frame.setVisible(true);
    }
    
}
