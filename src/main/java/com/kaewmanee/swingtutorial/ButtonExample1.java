package com.kaewmanee.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
public class ButtonExample1 extends JFrame{
    JButton button;
    JTextField text;
    JButton clearButton;
    public ButtonExample1(){
        super("Super Button Example");
        button = new JButton("Welcome");
        button.setBounds(50,100,120,30);
        button.setIcon(new ImageIcon("welcome.png"));
        text = new JTextField();
        text.setBounds(120,50,120,20);
        button.addActionListener(new ActionListener() {
            //anonymous object
            @Override 
            public void actionPerformed(ActionEvent e) {
                text.setText("welcome to Buu"); 
                
            }
            
        });
        clearButton = new JButton("clear");
        clearButton.setBounds(200,100,95,30);
        clearButton.setIcon(new ImageIcon("cleaning.png"));        
        clearButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                text.setText("");
                
            }
            
        });
        this.setSize(400,500);
        this.add(text);
        this.add(button);
        this.add(clearButton);
        this.setLayout(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        ButtonExample1 frame = new ButtonExample1();
        
    }
    
}
