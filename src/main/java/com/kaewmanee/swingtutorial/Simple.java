package com.kaewmanee.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

class MyApp{ //สร้างในfile เดียวกันได้แต่ต้องไม่มีpublic
        JFrame frame; //ใส่หรือไม่ใส่privateก็ได่
        JButton button;
        public MyApp(){ 
            frame = new JFrame("First JFrame");
            button = new JButton("Click");
            button.setBounds(180,100,100,40); //กว้าง 100 สูง 40
            frame.setLayout(null);
            frame.add(button);
            frame.setSize(500,500);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        }
    }
public class Simple{
    public static void main(String[] args) {
        MyApp app = new MyApp();
    }
}
    
    

